;;; This is a template operating system configuration for the build nodes
;;; behind <https://guix.bordeaux.inria.fr>.  It should be customized through
;;; inheritance.

(use-modules (gnu)
             (srfi srfi-1))
(use-service-modules admin avahi base cuirass mcron networking
                     ssh virtualization)
(use-package-modules certs screen ssh)
(use-modules ((cuirass-package) #:prefix latest:))


(define %custom-base-services
  (modify-services %base-services
    (guix-service-type config =>
                       (guix-configuration
                        (inherit config)
                        (use-substitutes? #t)
                        (substitute-urls '("https://guix.bordeaux.inria.fr"
                                           "https://ci.guix.gnu.org"
                                           "https://bordeaux.guix.gnu.org"))
                        (authorized-keys
                         ;; Authorize substitutes from Ludo's laptop to allow
                         ;; for offloading.
                         (append (list (local-file "./ribbon-export.pub")
                                       (local-file "guix/keys/guix-hpc1.pub")
                                       (local-file "guix/keys/guix-hpc3.pub")
                                       (local-file "guix/keys/guix-hpc4.pub")
                                       (local-file "guix/keys/guix-hpc5.pub")
                                       (local-file "guix/keys/guix-hpc6.pub")
                                       (local-file "guix/keys/guix-hpc7.pub"))
                                 (guix-configuration-authorized-keys config)))
                        (build-accounts 40)

                        ;; We have 24 to 32 cores.
                        (extra-options '("--max-jobs=4" "--cores=8"))))))

(define garbage-collector-jobs
  ;; Collect garbage several times a day, with a bigger collection at 5AM.
  (list #~(job '(next-hour '(5))
               "guix gc -F 50G -d 2m")
        #~(job '(next-hour '(10))
               "guix gc -F 15G")
        #~(job '(next-hour '(15))
               "guix gc -F 25G")
        #~(job '(next-hour '(20))
               "guix gc -F 15G")))


(operating-system
  (host-name "guix-hpc-build-node-template")      ;placeholder
  (timezone "Europe/Paris")
  (locale "fr_FR.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets '("/boot/efi"))))

  (file-systems %base-file-systems)   ;must be configured through inheritance

  (users (append (load "admins.scm") %base-user-accounts))

  ;; Globally-installed packages.
  (packages (cons* screen
                   openssh                  ;so that 'scp' works
                   %base-packages))

  (services
   ;; Networking service must be added through inheritance.
   (append (list (service openssh-service-type
                          (openssh-configuration
                           ;; Allow root login with public key only to permit use
                           ;; of 'guix deploy'.
                           (permit-root-login 'prohibit-password)

                           (authorized-keys
                            `(("ludo" ,(local-file "ludo.pub"))
                              ("florent" ,(local-file "florent.pub"))
                              ("eagullo" ,(local-file "manu.pub"))
			      ("rgarbage" ,(local-file "romain.pub"))
                              ("root" ,(local-file "ludo.pub")
                                      ,(local-file "romain.pub"))))))

                 ;; Start a Cuirass "worker".
                 (service cuirass-remote-worker-service-type
                          (cuirass-remote-worker-configuration
                           (cuirass latest:cuirass)
                           (workers 4)
                           ;; XXX: We'd write "guix-hpc4.bordeaux.inria.fr"
                           ;; but currently we have to (1) provide and IPv4
                           ;; address, and (2) provide the port number.
                           (server "194.199.1.1:5555")
                           (systems (list "x86_64-linux"
                                          "aarch64-linux"
                                          "powerpc64le-linux"))))

                 (service avahi-service-type)     ;for Cuirass

                 (service ntp-service-type)
                 (service qemu-binfmt-service-type
                          (qemu-binfmt-configuration
                           (platforms (lookup-qemu-platforms "arm"
                                                             "aarch64"
                                                             "ppc64le"
                                                             "riscv64"))))

                 (simple-service 'gc-jobs mcron-service-type
                                 garbage-collector-jobs))
           %custom-base-services)))
