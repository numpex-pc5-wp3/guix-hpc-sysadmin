;;; Configuration of the guix-hpc7 build node.

(use-modules (gnu))

(let ((base-os (load "build-node.scm")))
  (operating-system
    (inherit base-os)
    (host-name "guix-hpc7")

    ;; This is a BIOS setup.
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (targets (list "/dev/sda"))))

    (initrd-modules (append (list "megaraid_sas")
                            %base-initrd-modules))

    (file-systems (append (list (file-system
                                  (mount-point "/")
                                  (device (uuid "3ad202ec-8f8b-4b78-a7f8-dd1fab1aa92c"
                                                'ext4))
                                  (type "ext4")))
                          %base-file-systems))

    (swap-devices (list (swap-space
                         (target (uuid "bc85fc8b-daf9-4edd-b717-83591355ceb6")))))

    (services
     ;; Configure networking.
     (cons  (service static-networking-service-type
                     (list (static-networking
                            (addresses (list (network-address
                                              (device "eno3")
                                              (value "194.199.1.27/27"))))
                            (routes (list (network-route
                                           (destination "default")
                                           (gateway "194.199.1.30"))))
                            (name-servers '("193.50.111.150")))))

            (operating-system-user-services base-os)))))
