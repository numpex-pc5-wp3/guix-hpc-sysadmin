;;; Configuration of the guix-hpc3 build node.

(use-modules (gnu))

(let ((base-os (load "build-node.scm")))
  (operating-system
    (inherit base-os)
    (host-name "guix-hpc5")

    ;; This is a UEFI setup.
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (targets '("/dev/sda"))))

    (initrd-modules (append (list "megaraid_sas")
                            %base-initrd-modules))

    (file-systems (append (list (file-system
                                  (mount-point "/")
                                  (device (uuid "d17c4651-b142-4802-9d70-b018ee72c58e"
                                                'ext4))
                                  (type "ext4")))
                          %base-file-systems))

    (swap-devices (list (swap-space
                         (target (uuid "33dbe483-bc2f-4299-9ace-bad414fdcafd")))))

    (services
     ;; Configure networking.
     (cons  (service static-networking-service-type
                     (list (static-networking
                            (addresses (list (network-address
                                              (device "eno3")
                                              (value "194.199.1.26/27"))))
                            (routes (list (network-route
                                           (destination "default")
                                           (gateway "194.199.1.30"))))
                            (name-servers '("193.50.111.150")))))

            (operating-system-user-services base-os)))))
