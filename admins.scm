;; User accounts for system adminstrators (sudoers).

(let ((account (lambda (name full-name)
                 (user-account
                  (name name)
                  (comment full-name)
                  (group "users")
                  (supplementary-groups '("wheel" ;sudo
                                          "netdev"))))))
  (list (account "ludo" "Ludovic Courtès")
        (account "florent" "Florent Pruvost")
        (account "eagullo" "Emmanuel Agullo")
        (account "jlelaura" "Julien Lelaurain")
	(account "rgarbage" "Romain Garbage")))
