# Environment modules for PlaFRIM

This directory contains Guix *channel* and *manifest* files to generate
environment modules for [PlaFRIM](https://www.plafrim.fr).  To create or
update modules, run:

```
guix shell guix guix-modules -- \
  guix time-machine -C channels.scm -- \
  module create -m manifest.scm --tune=ivybridge \
  -o /cm/shared/dev/modules/guix
```

The operation is transactional and previous generations of the module
set remain available under `/var/guix/profiles/per-user/$USER/modules/`,
so this could just as well be run from a cron job.

For more information on Guix-generated modules, check out
[Guix-Modules](https://gitlab.inria.fr/guix-hpc/guix-modules) and [this
blog
post](https://hpc.guix.info/blog/2022/05/back-to-the-future-modules-for-guix-packages/).
