;;; Channels for use with 'manifest.scm'.

(append (list (channel
               (name 'guix-hpc)
               (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
               (branch "master"))
              (channel
               (name 'guix-hpc-non-free)
               (url "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free.git")
               (branch "master")))
        %default-channels)
