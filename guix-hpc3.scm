;;; Configuration of the guix-hpc3 build node.

(use-modules (gnu))

(let ((base-os (load "build-node.scm")))
  (operating-system
    (inherit base-os)
    (host-name "guix-hpc3")

    ;; This is a BIOS setup.
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (targets (list "/dev/sda"))))

    (initrd-modules (append (list "megaraid_sas")
                            %base-initrd-modules))

    (file-systems (append (list (file-system
                                  (mount-point "/")
                                  (device (uuid "cdbcb919-535b-41a7-9387-181a2bed7d1d"
                                                'ext4))
                                  (type "ext4")))
                          %base-file-systems))

    (swap-devices (list (swap-space
                         (target (uuid "cc80bc00-a9e8-454e-80ad-68251287b80e")))))

    (services
     ;; Configure networking.
     (cons  (service static-networking-service-type
                     (list (static-networking
                            (addresses (list (network-address
                                              (device "eno1")
                                              (value "194.199.1.17/27"))))
                            (routes (list (network-route
                                           (destination "default")
                                           (gateway "194.199.1.30"))))
                            (name-servers '("193.50.111.150")))))

            (operating-system-user-services base-os)))))
