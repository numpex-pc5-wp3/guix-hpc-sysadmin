;;; Configuration of the guix-hpc6 build node.

(use-modules (gnu))

(let ((base-os (load "build-node.scm")))
  (operating-system
    (inherit base-os)
    (host-name "guix-hpc6")

    ;; This is a UEFI setup.
    (bootloader (bootloader-configuration
                  (bootloader grub-efi-bootloader)
                  (targets (list "/boot/efi"))))

    (initrd-modules (append (list "megaraid_sas")
                            %base-initrd-modules))

    (file-systems (append (list (file-system
                                  (mount-point "/boot/efi")
                                  (device (uuid "D707-02D1" 'fat32))
                                  (type "vfat"))
                                (file-system
                                  (mount-point "/")
                                  (device (uuid "854db524-6b7e-442b-bcf7-daee27bbf5fd"
                                                'ext4))
                                  (type "ext4")))
                          %base-file-systems))

    (swap-devices (list (swap-space
                         (target (uuid "349fbf68-f690-43cf-95fb-9304ddd42079")))))

    (services
     ;; Configure networking.
     (cons  (service static-networking-service-type
                     (list (static-networking
                            (addresses (list (network-address
                                              (device "eno3")
                                              (value "194.199.1.7/27"))))
                            (routes (list (network-route
                                           (destination "default")
                                           (gateway "194.199.1.30"))))
                            (name-servers '("193.50.111.150")))))

            (operating-system-user-services base-os)))))
