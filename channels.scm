(cons* (channel
        (name 'cuirass)
        (url "https://git.savannah.gnu.org/git/guix/guix-cuirass.git")
        (branch "main")
        (introduction
         (make-channel-introduction
          "c75620777c33273fcd14261660288ec1b2dc8123"
          (openpgp-fingerprint
           "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
       %default-channels)
